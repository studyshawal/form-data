const multer = require('multer');
var fs = require("fs").promises;
var path = require("path");
//mongo db 
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'Module_Company';
var db;
// Create a new MongoClient
const client = new MongoClient(url,{ useUnifiedTopology: true });
// Use connect method to connect to the Server
client.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to the DB server.");
    db = client.db(dbName);
  });

// declaring the express parameters
var express = require("express");
var bodyParser = require('body-parser')
var path = require("path");


//express definition
var app = express();
var port = 8000;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false })) 
// parse application/json
app.use(bodyParser.json())
// path of the server directory
app.use(express.static("./")); 

// adding a user
app.post("/adduser",function(req,res){
    userdata= {
        _id : req.body.userid,
        Name : req.body.name
        
    }
     //find if a value exists
    db.collection("users").find({ "_id": userdata._id }, { $exists: true }).toArray(function(err, doc){
        if (doc && doc.length){
            return res.json({message : `The user ID ${userdata._id} already exists.`}); // print out what it sends back
        }
        else {
    // inserting the data into the database
    db.collection("users").insertOne(userdata, function(err, resultObject) {
        if(!resultObject.result.ok || !resultObject.result.n || err) {
            return res.json({message: "Error", response : false});
        }
        res.json({message :`The user has been added`});
    });
    }
    })
})  

// adding a company
app.post("/addcompany",function(req,res){
    companydata= {
        _id : req.body.companyid,
        UserID : req.body.userid,   
        CompanyName : req.body.companyname,
        Companydescription : req.body.companydescription
    }
    //find if the user exists
    db.collection("users").find({"_id" : companydata.UserID}, {$exists : true}).toArray(function(e,d){
        if(d && d.length){
    //find if a company exists
    db.collection("company").find({ "_id" : companydata._id }, { $exists: true }).toArray(function(err, doc){
        if (doc && doc.length){  //company exist check
            //returns false yani ke company exist karti hai cant insert another one 
            return res.json({message : "The company already exists , try another Company ID."});
        }
        else{            
            // company doesnt exist so inserting the data into the company collection of the database 
            db.collection("company").insertOne(companydata, function(err, resultObject) {
            //error handling if the record has been inserted or not
                if(!resultObject.result.ok || !resultObject.result.n || err) {
                    // return false means that theres been an error
                    return res.json({message: "Error", response : false});
                }
            // return ture means that the company has been added successfully 
            res.json({message : "The company has been added."});
        });
    }})
        }
        else{
            // returning false yani ke user id doesnt exists
            return res.json({message : "The user doesn't exist, so the company can't be added."});
        }
})
});

var upload = multer().single('myFile');
app.use(upload);
//multer for module uploading 
app.post("/moduleupload",function(req,res){
    //creating obj for module data
    moduledata = {
        _id :  req.body.moduleid,
        CompanyID : req.body.companyid,
        Modulename : req.body.modulename,
        Moduledescription : req.body.moduledescription,
        // Filename : req.body.filename
    }
    //checking if the company is there or not
    db.collection("company").find({"_id" : req.body.companyid}, {$exists : true}).toArray(function(err,d){
        // ager company hai tou check ke module name pehle se to nae hai 
        if(d && d.length){
            //checking if the module already exits or not
            db.collection("module").find({"_id" : req.body.moduleid}, {$exists : true}).toArray(function(error,doc){
                // ager pehle se exists tou if response
                if(doc && doc.length){
                    res.json(
                        {message : `The Module ID "${req.body.moduleid}" already exists, try another ID and upload again.`});
                }
                else{
                    // company doesnt exist so inserting the data into the company collection of the database 
                    db.collection("module").insertOne(moduledata, function(merr, mresult) {
                //error handling if the record has been inserted or not
                    if(!mresult.result.ok || !mresult.result.n || merr) {
                        // return false means that theres been an error
                        return res.json({message: "Error", response : false});
                    }
                // module has been added in else 
                    else{
                    res.json(
                    {message : `The module "${req.body.modulename}" with module ID "${req.body.moduleid}" has been added.`});      
                }
            });
                }
            })
        }
        // else caters the response for sending the message that company hee exist nahe karti
        else{
            res.json(
            {message : `The company ID "${req.body.companyid}" under which you are trying to upload the module doesn't exist.`});
        
        }
    })  
})

// file uploading 
app.post('/uploadfile', async (req, res, next) => {
    db.collection("company").find({"_id" : req.body.companyid}, {$exists : true}).toArray(function(err,d){
        if(d && d.length){
            db.collection("module").find({"_id" : req.body.moduleid}, {$exists : true}).toArray(async function(error,doc){
                if(doc && doc.length){
                    const file = req.file;
                    const ext = path.extname(file.originalname);
                    await fs.writeFile(`./uploads/${req.body.moduleid}.${ext}`, file);
                    if (!file) {
                        const error = new Error('Please upload a file');
                        error.httpStatusCode = 400;
                        return next(error);
                    }
                        res.send(file);
                }
                else{
                    res.json({message : " Module already exists, file not uploaded."});                  
                }
            })
        }
        else{
            res.json({message : "Company doesn't exist, file not uploaded"});
        }
    })   
})

//serch module
app.post('/search',function(req,res){
    db.collection("company").find({"_id" : req.body.search}, {$exists : true}).toArray(function(err,doc){
        if(doc && doc.length){
            db.collection("module").find({"CompanyID" : req.body.search}).toArray(function(e,d){
                if(d && d.length){
                    res.json({message :  d, status : true});                }
                else{
                    res.json({message : "No modules for the searched company.",status : false})
                }
            })
        }
        else{
            res.json({message : "The company doesn't exist.",status : false})
        }
    })
})
// Error Handling for Express server
app.use(function(req, res) {
    console.log(`Error ${req.url}`);
    res.status(404).json({code:"Error", message: `Not found Link ${req.url}`})
});

// Start the Server
app.listen(port, function() {
    console.log("Server started.");
    console.log(`Listening on the port number: ${port}`)
});